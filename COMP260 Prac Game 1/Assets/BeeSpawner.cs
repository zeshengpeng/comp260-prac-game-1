﻿using UnityEngine;
using System.Collections;

public class BeeSpawner : MonoBehaviour {
	public BeeMove beeprefab;

	public int nBees ;
	public float xMin, yMin;
	public float width, height;
	public Transform player1;
	public Transform player2;
	public ParticleSystem ex1;
	public float minSpeed, maxSpeed;
	public float minTurnSpeed, maxTurnSpeed;
	private Transform target;
	private Vector2 heading;
	private float speed;
	private float turnSpeed;
	// Use this for initialization
	void Start () {
		
		for (int i = 0; i < nBees; i++) {
			BeeMove bee = Instantiate (beeprefab);
			bee.transform.parent = transform;
			bee.gameObject.name = "Bee " + i;
			float x = xMin + Random.value * width;
			float y = yMin + Random.value * height;
			bee.transform.position = new Vector2 (x, y);
			bee.target1 = player1;
			bee.target2 = player2;
			bee.explosionPrefab = ex1;

		}

		playerMove p = FindObjectOfType<playerMove> ();
		target = p.transform;
		heading = Vector2.right;
		float angle = Random.value * 360;
		heading = heading.Rotate (angle);
		maxSpeed = Mathf.Lerp (minSpeed, maxSpeed, Random.value);
		turnSpeed = Mathf.Lerp (minTurnSpeed, maxTurnSpeed, Random.value);


	}


	public void DestroyBees(Vector2 centre, float radius){
		for (int i = 0; i < transform.childCount; i++) {
			Transform child = transform.GetChild (i);
			Vector2 v = (Vector2)child.position - centre;
			if (v.magnitude <= radius) {
				Destroy (child.gameObject);
			}
		}
	}
	// Update is called once per frame
	void Update () {
	
	}
}
