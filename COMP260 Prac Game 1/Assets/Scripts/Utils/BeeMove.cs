﻿using UnityEngine;
using System.Collections;

public class BeeMove : MonoBehaviour {
	public float speed = 4.0f;
	public float turnSpeed = 180.0f;
	public Vector2 heading = Vector3.right;
	public Transform target1;
	public Transform target2;
	public ParticleSystem explosionPrefab;
	// Use this for initialization


	void Update () {
		
		Vector2 direction;
		float angle = turnSpeed * Time.deltaTime;
		Vector2 direction1=target1.position - transform.position;
		Vector2 direction2=target2.position - transform.position;
		float sqrlen1 = direction1.sqrMagnitude;
		float sqrlen2 = direction2.sqrMagnitude;
		if (sqrlen1 < sqrlen2) {
			direction = target1.position - transform.position;
		} else {
			direction = target2.position - transform.position;
		}

		if (direction.IsOnLeft (heading)) {
			heading = heading.Rotate (angle);
		
		} else {
		
			heading = heading.Rotate (-angle);
		}
		
		transform.Translate (heading * speed * Time.deltaTime);
	}
	void OnDrawGizmos(){
		Gizmos.color = Color.red;
		Gizmos.DrawRay (transform.position, heading);
		Gizmos.color = Color.yellow;
		//Vector2 direction = target.position - transform.position;
		//Gizmos.DrawRay (transform.position, direction);

	}
	void OnDestroy(){
		ParticleSystem explosion = Instantiate (explosionPrefab);
		explosion.transform.position = transform.position;
		Destroy (explosion.gameObject, explosion.duration);
	
	}
}
