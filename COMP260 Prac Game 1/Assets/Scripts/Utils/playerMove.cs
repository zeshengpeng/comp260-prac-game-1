﻿using UnityEngine;
using System.Collections;

public class playerMove : MonoBehaviour {
	public Vector2 move;
	public Vector2 velocity;
	public float maxSpeed = 5.0f;
	private BeeSpawner beeSpawner;
	public float destroyRadius;
	// Use this for initialization
	void Start(){
		beeSpawner = FindObjectOfType<BeeSpawner> ();
	}
	
	// Update is called once per frame
	void Update () {
		if(transform.name == "player 1"){
			Vector2 direction;
			direction.x = Input.GetAxis ("Horizontal");
			direction.y = Input.GetAxis ("Vertical");
			Vector2 velocity = direction * maxSpeed;
			transform.Translate (velocity * Time.deltaTime);
		}
		if(transform.name == "player 2"){
			Vector2 direction;
			direction.x = Input.GetAxis ("Horizontal2");
			direction.y = Input.GetAxis ("Vertical2");
			Vector2 velocity = direction * maxSpeed;
			transform.Translate (velocity * Time.deltaTime);
		}
		if (Input.GetButton ("Fire1")) {
			beeSpawner.DestroyBees (transform.position,destroyRadius);
		}
	}
}
